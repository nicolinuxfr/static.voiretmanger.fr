
<!DOCTYPE html>
<head>
<base href="https://static.voiretmanger.fr" />


<meta charset="UTF-8">

<meta name="apple-mobile-web-app-title" content="Voir et manger" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover"> 
<meta property="fb:pages" content="384298108368" />

<link href='https://static.voiretmanger.fr/contenu/ui/theme/css/font.css' rel='stylesheet' type='text/css'>
<link rel="stylesheet" media="all" href="https://static.voiretmanger.fr/contenu/ui/theme/style.css" />


<link rel="shortcut icon" href="https://static.voiretmanger.fr/contenu/favicon.ico" />
<link rel="mask-icon" href="https://static.voiretmanger.fr/contenu/icon.svg" color="rgb(197, 31, 22)">

		<link rel="preload" href="https://static.voiretmanger.fr/contenu/donnees/2013/06/l-inconnu-du-lac-alain-guiraudie.jpg" as="image">
	<link rel="preload" href="https://static.voiretmanger.fr/contenu/ui/theme/css/playfair-display-v10-latin-regular.woff2" as="font" type="font/woff2" crossorigin>
	<link rel="preload" href="https://static.voiretmanger.fr/contenu/ui/theme/css/playfair-display-v10-latin-700italic.woff2" as="font" type="font/woff2" crossorigin>






<!-- This site is optimized with the Yoast SEO plugin v7.6.1 - https://yoast.com/wordpress/plugins/seo/ -->
<title>L&#039;Inconnu du lac, Alain Guiraudie - À voir et à manger</title>
<meta name="description" content="L&#039;Inconnu du lac est un film minimaliste par son dispositif et sa mise en scène (un huis clos autour d&#039;un lac, trois personnages), mais qui s&#039;avère extrêmement riche, peut-être justement par son minimalisme. C&#039;est en tout cas un film passionnant, à ne pas rater pour plein de raisons et certainement pour ses scènes de sexe explicites…"/>

<meta property="og:locale" content="fr_FR" />
<meta property="og:type" content="article" />
<meta property="og:title" content="L&#039;Inconnu du lac, Alain Guiraudie - À voir et à manger" />
<meta property="og:description" content="L&#039;Inconnu du lac est un film minimaliste par son dispositif et sa mise en scène (un huis clos autour d&#039;un lac, trois personnages), mais qui s&#039;avère extrêmement riche, peut-être justement par son minimalisme. C&#039;est en tout cas un film passionnant, à ne pas rater pour plein de raisons et certainement pour ses scènes de sexe explicites…" />
<meta property="og:url" content="https://static.voiretmanger.fr/inconnu-lac-guiraudie/" />
<meta property="og:site_name" content="À voir et à manger" />
<meta property="article:publisher" content="https://www.facebook.com/voiretmanger/" />
<meta property="article:tag" content="Amour" />
<meta property="article:tag" content="Drame" />
<meta property="article:tag" content="Enquête" />
<meta property="article:tag" content="Homosexualité" />
<meta property="article:tag" content="Huis clos" />
<meta property="article:tag" content="Police" />
<meta property="article:tag" content="Sexe" />
<meta property="article:tag" content="Thriller" />
<meta property="article:section" content="À voir" />
<meta property="article:published_time" content="2013-06-16T11:37:13+00:00" />
<meta property="article:modified_time" content="2017-08-09T14:53:41+00:00" />
<meta property="og:updated_time" content="2017-08-09T14:53:41+00:00" />
<meta property="og:image" content="https://static.voiretmanger.fr/contenu/donnees/2013/06/l-inconnu-du-lac-alain-guiraudie-1500x1000.jpg" />
<meta property="og:image:secure_url" content="https://static.voiretmanger.fr/contenu/donnees/2013/06/l-inconnu-du-lac-alain-guiraudie-1500x1000.jpg" />
<meta property="og:image:width" content="1500" />
<meta property="og:image:height" content="1000" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="L&#039;Inconnu du lac est un film minimaliste par son dispositif et sa mise en scène (un huis clos autour d&#039;un lac, trois personnages), mais qui s&#039;avère extrêmement riche, peut-être justement par son minimalisme. C&#039;est en tout cas un film passionnant, à ne pas rater pour plein de raisons et certainement pour ses scènes de sexe explicites…" />
<meta name="twitter:title" content="L&#039;Inconnu du lac, Alain Guiraudie - À voir et à manger" />
<meta name="twitter:site" content="@nicolinuxfr" />
<meta name="twitter:image" content="https://static.voiretmanger.fr/contenu/donnees/2013/06/l-inconnu-du-lac-alain-guiraudie.jpg" />
<meta name="twitter:creator" content="@nicolinuxfr" />
<script type='application/ld+json'>{"@context":"https:\/\/schema.org","@type":"Person","url":"https:\/\/static.voiretmanger.fr\/","sameAs":["https:\/\/www.facebook.com\/voiretmanger\/","https:\/\/twitter.com\/nicolinuxfr"],"@id":"#person","name":"Nicolas Furno"}</script>
<!-- / Yoast SEO plugin. -->

<link rel='dns-prefetch' href='//s0.wp.com' />


<link rel='stylesheet' id='dashicons-css'  href='https://static.voiretmanger.fr/inc/css/dashicons.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='jetpack_css-css'  href='https://static.voiretmanger.fr/contenu/lib/jetpack/css/jetpack.css' type='text/css' media='all' />
<script type='text/javascript' src='https://static.voiretmanger.fr/inc/js/jquery/jquery.js'></script>
<script type='text/javascript' src='https://static.voiretmanger.fr/inc/js/jquery/jquery-migrate.min.js'></script>






		<style type="text/css">
						ol.footnotes li {list-style-type:decimal;}
								</style>
		
<style type='text/css'>
article .post-content a:hover, .post-meta ul a:hover, .post-meta .resto a:hover, .post-meta ul strong {color: #413731;}
        .partage ul li, .partage h4 {background-color: #413731;}
</style>
<meta name="theme-color" content="#413731" />
<script type="text/javascript">var algolia = {"debug":false,"application_id":"UED6ZHIEOG","search_api_key":"740c6ae0383cf9458b08e0a5e07080d0","powered_by_enabled":true,"query":"","autocomplete":{"sources":[],"input_selector":"input[name='s']:not('.no-autocomplete')"},"indices":{"searchable_posts":{"name":"wp_searchable_posts","id":"searchable_posts","enabled":true,"replicas":[]}}};</script><link rel="icon" href="https://static.voiretmanger.fr/contenu/donnees/2017/07/cropped-logo-32x32.png" sizes="32x32" />
<link rel="icon" href="https://static.voiretmanger.fr/contenu/donnees/2017/07/cropped-logo-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://static.voiretmanger.fr/contenu/donnees/2017/07/cropped-logo-180x180.png" />
<meta name="msapplication-TileImage" content="https://static.voiretmanger.fr/contenu/donnees/2017/07/cropped-logo-270x270.png" />
</head>

<body>



		<header class="header-post">
			<div class="logo">
				<a href="https://static.voiretmanger.fr/" title="Retour à la page d'accueil" rel="home"><img src="https://static.voiretmanger.fr/contenu/ui/theme/logo.svg" alt="" width="100px" scale="0"></a></div>
	
	<div class="recherche" id="recherche" onclick="document.getElementById('search').focus();">
		<input type="checkbox" id="op"></input>
	<div class="lower">
		<label id="rechercher" for="op"><span class="dashicons dashicons-search"></span></label>
	</div>


	<div class="overlay overlay-hugeinc">
		<div class="logo">
			<a href="#" onclick="$('#op').prop('checked', false); document.getElementById('search').blur();"><img src="https://static.voiretmanger.fr/contenu/ui/theme/logo.svg" alt="" width="100px" scale="0"></a></div>
			
			<form method="get" id="searchform"  action="https://static.voiretmanger.fr/"> 
	<input class="case" type="text" autofocus id="search" name="s" value="" placeholder="" /> 
	<input type="hidden" id="searchsubmit" /> 
</form>

<nav>
	<ul id="results"></ul>
</nav>
	</div>
</div>

 			</header><!-- #header -->

	<main class="post-template-default single single-post postid-9741 single-format-standard" >
		<div id="container">
			<div id="content" role="main">


	<article id="post-9741" class="single">
			
				
		
		<header class="post-header" style="background-image: url(https://static.voiretmanger.fr/contenu/donnees/2013/06/l-inconnu-du-lac-alain-guiraudie.jpg);  background-position:  ;">
		<div class="flou">
			<h2 class="post-title"><em>L&rsquo;Inconnu du lac</em>, Alain Guiraudie</h2>
		</div>
		</header>

		<section class="post-content">
			<p>Rendu célèbre par la polémique suscitée par son affiche, <em>L’Inconnu du lac</em> est intéressant pour plus que cela. Le dernier long-métrage d’Alain Guiraudie se déroule sur les bords d’un lac, un lieu naturiste de drague homosexuelle et si le sexe y est bien présent, ce n’est absolument pas un film pornographique pour autant. Ce huis clos tend en fait, étonnamment, au thriller psychologique, ou encore au conte fantastique. Le cinéaste fait de l’économie de moyens de ce film très minimaliste une richesse, pour un résultat vertigineux et passionnant. Un très beau film, à ne pas rater.</p>
<div style="text-align:center;"><a href="http://www.allocine.fr/film/fichefilm_gen_cfilm=212434.html"><img class="aligncenter" src="https://static.voiretmanger.fr/contenu/donnees/2013/06/l-inconnu-du-lac-guiraudie.jpg" alt="L inconnu du lac guiraudie" title="l-inconnu-du-lac-guiraudie.jpg" width="100%" /></a></div>
<p>Alain Guiraudie filme l’arrivée de Franck comme un rituel immuable. Chaque jour, au volant de sa Renault 25, il vient se garer sur un parking sauvage, sort de la voiture pour rejoindre la plage de galets au bord d’un lac. Une petite dizaine d’hommes, nus pour la plupart, l’y attendent et se retournent pour jauger le nouvel arrivant. Franck pose sa serviette, se déshabille et profite lui aussi du soleil, de l’eau du lac et… du corps d’un homme de temps en temps. <em>L’Inconnu du lac</em> se déroule exclusivement au bord de ce lac naturiste qui est aussi un lieu de drague gay. On n’en sort jamais et tout ce qui est extérieur au lac n’est que mentionné, on ne le voit jamais et cela n’a pas tellement d’importance. L’essentiel, c’est ce qui se déroule sur cette petite plage et le film d’Alain Guiraudie est d’abord construit de rencontres et de dialogues. Franck connaît quelques habitués, certains qu’il salue d’un mot, d’autres qu’il embrasse. Le lieu précis n’est jamais mentionné, mais c’est un petit monde où tout le monde se connaît un peu. En cette période estivale, il y a deux nouveaux parmi les habitués. Henri d’abord, la quarantaine, reste à l’écart du groupe, il ne se déshabille pas, ne se baigne pas et ne semble pas chercher la compagnie d’autres hommes. Franck vient malgré tout le voir, par curiosité, puis par sympathie pour cet homme manifestement mal à l’aise avec ses rondeurs, un peu bougon. Michel ensuite, qui pourrait être l’extrême opposé : bien fichu, bronzé, il exhibe fièrement ses attributs et c’est le coup de foudre pour Franck. Puisqu’il s’agit d’un lieu de drague, le personnage principal de <em>L’Inconnu du lac</em> essaie de l’approcher pour du sexe, certes, mais il espère aller plus loin. </p>
<p>Le cérémonial est bien en place — on voit Franck arriver en voiture une dizaine de fois pendant tout le film —, mais il est perturbé par un évènement tragique. Un jour, un homme disparaît et la police ne tarde pas à venir s’intéresser à ce lieu de drague notoire où il aurait pu mourir, noyé ou assassiné. Pendant son heure et demie, <em>L’Inconnu du lac</em> bascule du côté du thriller, ou du moins du film policier avec cette enquête menée par un enquêteur coriace qui sent que les habitués du lieu ne lui disent pas tout. Ce serait assez classique, si Alain Guiraudie n’avait pas la malice de montrer ce qui s’est passé à son personnage principal, et aux spectateurs par la même occasion. Dès le deuxième jour, Franck reste sur place pour observer discrètement l’objet de ses désirs qui nage avec son amant du moment. La nuit commence à tomber, mais il aperçoit très bien que Michel enfonce et maintient la tête de son compagnon jusqu’à disparition du corps sous l’eau. On sait alors, autant que Franck, que c’est un type dangereux, mais cela ne va pas empêcher le personnage principal de <em>L’Inconnu du lac</em> de poursuivre sa relation avec lui. À partir de ce moment, l’ambiance change dans le film. Commencé sur le ton du badinage et du sexe sans lendemain, le climat s’y fait plus oppressant et une sorte de menace vient se poser sur les bords du lac. La nature très présente — le vert et le bleu dominent dans le film, en plus du rose naturellement — se fait plus menaçante et Alain Guiraudie le rend très bien en filmant pourtant toujours les mêmes arbres, les mêmes herbes hautes, les mêmes galets. La mise en scène suffit, sans la moindre musique d’ailleurs, à passer d’une vue tranquille et reposante à une vue menaçante où le spectateur sent que quelque chose de tragique pourrait arriver. L’inspecteur n’est pas le plus dangereux dans ce contexte : quand la mort est passée par là, tout devient menace potentielle, même une question apparemment banale sur la Renault 25 de Franck, posée par un Michel qui cherche peut-être à sympathiser, mais surtout à savoir s’il a été vu. </p>
<p><em>L’Inconnu du lac</em> est vraiment remarquable de finesse dans sa manière de filmer l’arrivée d’un danger sur la plage et le lac, c’est aussi un film sur l’amour et le sexe. L’amour est plus fort que la menace et après avoir découvert la vérité, Franck désire toujours autant Michel. Ce dernier accepte justement de coucher avec lui, mais sans l’aimer pour autant. Alain Guiraudie filme une déception amoureuse : l’un aimerait aller plus loin et voir son amoureux loin du lac, en ville ou dans son lit ; l’autre, mystérieux, ne veut pas entendre parler d’une relation approfondie et préfère en rester aux bosquets du bois près du lac. Leur relation est torride et le cinéaste n’en perd pas une goutte — l’interdiction aux moins de 16 ans du film n’est pas de trop —, mais ce n’est que du sexe. Une forme de triangle amoureux se met alors en place entre Franck, Michel et Henri qui ne cherche pas de sexe, lui, mais qui tombe amoureux de Franck. Ce personnage est peut-être le plus beau de <em>L’Inconnu du lac</em>, le plus touchant sans doute. On sent parfaitement sa solitude et on sent qu’elle lui pèse, mais il ne semble rien faire pour la contrer. Il reste à l’écart, boudeur, mais c’est presque pour mieux être repéré par les autres. Et s’il n’est pas très bavard, il vise juste en posant les bonnes questions à Franck. Saluons d’ailleurs les choix judicieux d’Alain Guiraudie pour ses acteurs : Pierre Deladonchamps, Christophe Paou et Patrick d’Assumçao sont tous les trois excellents dans leurs rôles. </p>
<div style="text-align:center;"><img class="aligncenter" src="https://static.voiretmanger.fr/contenu/donnees/2013/06/l-inconnu-du-lac-pierre-deladonchamps-christophe-paou.jpg" alt="L inconnu du lac pierre deladonchamps christophe paou" title="l-inconnu-du-lac-pierre-deladonchamps-christophe-paou.jpg" width="100%" /></div>
<p>Avec des hommes nus qui s’aiment et baisent, Alain Guiraudie crée un film extrêmement riche. C’est peut-être parce qu’il est minimaliste que <em>L’Inconnu du lac</em> s’avère finalement aussi complexe : la simplicité formelle permet aux cinéastes de faire un film sur l’amour en même temps qu’un thriller où la menace pèse sur tous les personnages. On ne l’a pas évoqué, mais on pourrait aussi parler de l’humour présent dans le film, un humour noir plein de sarcasmes, mais qui ajoute encore une richesse à l’œuvre, surtout avec l’hilarant personnage de l’inspecteur. Impossible de résumer <em>L’Inconnu du lac</em> aux scènes de sexe explicites qu’il contient, c’est une œuvre bien plus riche et qui mérite d’être vue pour bien d’autres raisons…</p>
<div class="amazon">
<h3>Vous voulez m&rsquo;aider ?</h3>
<ul>
<li><a href="http://www.amazon.fr/gp/product/B00E8WL48S/ref=as_li_ss_tl?ie=UTF8&#038;tag=leblogdenic07-21&#038;linkCode=as2&#038;camp=1642&#038;creative=19458&#038;creativeASIN=B00E8WL48S">Acheter le film en DVD sur Amazon</a></li>
<li><a href="https://itunes.apple.com/fr/movie/linconnu-du-lac/id754631253">Acheter ou louer le film sur l&rsquo;iTunes Store</a></li>
</ul>
</div>
		</section>

								
		<footer class="post-footer">
			

<div data-no-instant>
</div>




<!-- Fonctions de partage -->
		
<section class="partage">
	<a title="Me contacter" href="mailto:nicolinux@gmail.com?subject=Au%20sujet%20de%20L&rsquo;Inconnu du lac, Alain Guiraudie - https://static.voiretmanger.fr/inconnu-lac-guiraudie/" ><h4>Une erreur, une remarque&nbsp;?</h4></a>
		<ul>
			<a title="Envoyer cet article par mail" href="mailto:?subject=L&rsquo;Inconnu du lac, Alain Guiraudie&amp;body=L&rsquo;Inconnu du lac, Alain Guiraudie - https://static.voiretmanger.fr/inconnu-lac-guiraudie/"><li>Envoyer par Mail</li></a>
			<a title="Partager cet article sur Twitter" href="http://twitter.com/share du lac, Alain Guiraudie"><li>Partager sur Twitter</li></a>
			<a title="Partager cet article sur Facebook" href="https://www.facebook.com/sharer/sharer.php"><li>Partager sur Facebook</li></a>
		</ul>
</section>

<section class="post-meta">
<!-- Taxonomie perso, articles liés et autres articles dans la catégorie -->
	<ul>
						
						<li><strong>R&eacute;alisateur</strong> : <a href="https://static.voiretmanger.fr/createur/alain-guiraudie/" rel="tag">Alain Guiraudie</a></li>						
			<li><strong>Ann&eacute;e</strong> : <a href="https://static.voiretmanger.fr/annee/2013/" rel="tag">2013</a></li>			<li><strong>Nationalit&eacute;</strong> : <a href="https://static.voiretmanger.fr/pays/france/" rel="tag">France</a></li>			
			<li class="acteurs"><strong>Acteurs</strong> : <a href="https://static.voiretmanger.fr/acteur/christophe-paou/" rel="tag">Christophe Paou</a>, <a href="https://static.voiretmanger.fr/acteur/jerome-chappatte/" rel="tag">Jérôme Chappatte</a>, <a href="https://static.voiretmanger.fr/acteur/patrick-dassumcao/" rel="tag">Patrick d'Assumçao</a>, <a href="https://static.voiretmanger.fr/acteur/pierre-deladonchamps/" rel="tag">Pierre Deladonchamps</a></li>												
						
			<li><strong>Tags</strong> : <a href="https://static.voiretmanger.fr/tag/amour/" rel="tag">Amour</a>, <a href="https://static.voiretmanger.fr/tag/drame/" rel="tag">Drame</a>, <a href="https://static.voiretmanger.fr/tag/enquete/" rel="tag">Enquête</a>, <a href="https://static.voiretmanger.fr/tag/homosexualite/" rel="tag">Homosexualité</a>, <a href="https://static.voiretmanger.fr/tag/huis-clos/" rel="tag">Huis clos</a>, <a href="https://static.voiretmanger.fr/tag/police/" rel="tag">Police</a>, <a href="https://static.voiretmanger.fr/tag/sexe/" rel="tag">Sexe</a>, <a href="https://static.voiretmanger.fr/tag/thriller/" rel="tag">Thriller</a></li>		</ul>
		
		
		<time class="post-date" datetime="16 juin 2013">
			
		
		Article publi&eacute; le <span style="display:inline;"> 16 juin 2013 (derni&egrave;re modification le 9 août 2017)</span>
	</time>
		
</section>		</footer>
			
	</article>


			</div><!-- #content -->
		</div><!-- #container -->

</main>


<footer class="site-footer">	
	<section class="menu"><a href="https://static.voiretmanger.fr/archives/">ARCHIVES</a><a href="https://static.voiretmanger.fr/a-propos/">À PROPOS</a></section>
	
	<section class="copyright">Publié depuis 2008. <a href="https://static.voiretmanger.fr/mentions-legales/">Tous droits réservés</a>.</section>
</footer><!-- #footer -->

	<script type='text/javascript' src='//static.voiretmanger.fr/contenu/ui/theme/js/search.js'></script>


	<script type='text/javascript' src='//static.voiretmanger.fr/contenu/ui/theme/js/bigfoot.min.js'></script>
		<link rel="stylesheet" media="all" href="//static.voiretmanger.fr/contenu/ui/theme/css/bigfoot-default.css" />
	<script type="text/javascript">
		$ = jQuery.noConflict();
		$.bigfoot({actionOriginalFN: "ignore"});
	</script>
	


<script type='text/javascript' src='https://s0.wp.com/contenu/js/devicepx-jetpack.js'></script>
<script type='text/javascript' src='https://static.voiretmanger.fr/contenu/lib/page-links-to/js/new-tab.min.js'></script>
</body>
</html>

<!-- Cache served by Simple Cache - Last modified: Sat, 16 Jun 2018 10:53:30 GMT -->
